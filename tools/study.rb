#!/usr/bin/env ruby 
#require 'wordle/word'
require 'pp'
require 'set'

class Word < String
  attr_reader :word
  def initialize(w)
    super
  end
  def key
    self.split('').uniq.sort.join
  end
  def to_a
    self.split('')
  end
  def regexp  
    Regexp.new("[%s]" % key)
  end
end

class Anagram < Set
  attr_reader :key
  def initialize(a)
    @size = 0
    super()
    @key = a
  end
  def add?(w)
    puts "add? %s" % w
    super
  end
  def add(w)
#    puts "add %s" % w
    @size += 1 if !include?(w)
    super
  end
  def pretty_print(q)
    q.text '<Ana '
    q.breakable
    q.text to_a.join(', ')
    q.breakable
    q.text ">"
  end
end

class Ana < Hash
  attr_reader :anagrams
  attr_reader :subanagrams
  attr_reader :alpha
  def initialize(size: 5)
    @size = size
    @anagrams = Hash.new { |h,k| h[k] = Anagram.new(k) }
    @subanagrams = Hash.new { |h,k| h[k] = Anagram.new(k) }
    @alpha = Hash.new { |h,k| h[k] = Array.new(5, 0) }
  end
  def to_s
    "<Ana %s>" % [ self.keys.join(', ') ]
  end
  def pretty_print(q)
    q.text '<Ana '
    q.breakable
    q.text '...>'
  end
  def add(w)
    raise "bad word" if w.length != 5
    if self.include? w
      warn "Dulicate %s" % w
    else
      word = Word.new(w)
      self[word] = word
      if (word.key.length == 5)
	@anagrams[word.key].add(word)
      else
        if word == 'whirr'
	  puts word.key
	  puts word
	end
	@subanagrams[word.key].add(word)
      end
      word.to_a.each_with_index do |l, i|
        @alpha[l][i] += 1
      end
    end
  end
  def score(w)
     word = Word.new(w)
     denominator = size
     t = 0
     a = Array.new(5, 0)
     word.to_a.each_with_index do |l, i|
       x = @alpha[l][i]
       a[i] += x
       t += Rational(x, denominator)
     end
     t
  end
end

ana = Ana.new
file = File.open '../data/wordlist';
file.each_line do |line|
  ana.add(line.chomp)
end

ana.anagrams.each_value do |a|
  next unless a.size > 3
  pp a
end

exit;
__END__
#puts ana.subanagrams.size

puts ana['whirr']
puts ana['whirl']
whirl = ana.anagrams[ana['whirl'].key]
whir = ana.subanagrams['hirw']

puts whirl.size
puts whir.size

puts ana.score('soled').to_f;
puts ana['cares'].key

#puts ana.score('soler').to_f;
#puts ana.score('carer').to_f;
#
pp ana.anagrams

#pp ana.each_value.inject(0) { |sum, w| sum += ana.score(w) }

x = Array.new(5,0)
ana.alpha.each do |k,v|
  v.each_with_index do |v,i|
    x[i] += v
  end
end

#ana.max_by(&:score)
puts "_____"

pp ana.anagrams['boy'].add? 'booby'
pp ana.anagrams['boy'];

puts "_____"

ana.each_value do |x|
  puts "%s %f" % [ x,  ana.score(x) ]
end

__END__

x = Hash.new { |h,k| h[k] = Array.new }
ana.each do |k,v|
  x[v.length].push k
end

pp x
x[8].each do |x|
  pp x
end
pp ana[x[8][0]]
pp ana[x[1][0]]

ana.eachkey do |w|
  c = 0
  words.each do |x|
    if w.regexp.match(x)
      c += 1
    end
  end
  puts "%s %7d %s" % [ w.word, c, w.key ]
end
#array = Array[1,1,1,1,1]
#pp Hash[('a'..'z').map { |s| [ s.to_sym, array] }]
#pp words

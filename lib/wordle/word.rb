require 'pp'

class Wordle

  class Word
    attr_reader :index
    attr_reader :letters
    attr_reader :values
    def initialize(word, index = nil)
      @word = word
      @letters = word.split(//)
      @index = index
      @values = Array.new(5, 0)
    end

    def value(reverse = false, vowels = false)
      ret = 0
      vs = 0
      begin
      rescue => e
         puts e.backtrace
         exit
      end
      if (vowels)
        vs =  @letters.select {  |v| v.match /[aeiouy]/ }.uniq.length
      end
      if (reverse)
        @rvalue ||= "%d%d%09d" % [ 5 - @letters.uniq.length, vs, @values.sum ]
        ret = @rvalue
      else
        @rvalue ||= "%d%d%09d" % [ @letters.uniq.length, 5 - vs, @values.sum ]
        ret = @rvalue
      end
      return ret
    end

    class Answers
    end

    def compare(answer)
      response = '-----'
      exact = answer[0..4].split(//).map { |l| l == '.' ? nil : l; }
      extra = Hash.new { |h,k| h[k] = 0 }
      answer[0..-1].split(//).each { |l| extra[l] += 1 }

      @letters.each_with_index do |l, i|
        if exact[i] == l
          response[i] = 'x'
          extra[l] -= 1
        end
      end
      @letters.each_with_index do |l, i|
#        puts "%s %s %s %s" % [ i, l, exact[i], extra[exact[i]] ]
        if response[i] == 'x'
        elsif extra[l] > 0
          response[i] = '+'
          extra[l] -= 1
        end
      end
      return response
    end

    def add_value(v, i, l = @letters[i])
      @values[i] = v
    end

    def set_value(v)
      @value = v
    end
#    def inspect
#       "'%s' %5s %5s [%s]" % [ @word, @rvalue, @nvalue, @values.map { |v| "%5d" %  v}.join(', ') ]
#    end
    def to_s
      @word
    end
    def to_a
      @letters
    end
    def chars
      @letters
    end
    def each_letter
      @letters.each do |l|
         yield l
      end
    end
    def each_letter_with_index
      @letters.each_with_index do |l, i|
        yield l, i
      end
    end
    def each_with_index
      @letters.each_with_index
    end
    def set_exclude
      @excluded = true
    end
    def set_hit
      raise "Bad input" if @state[i]
      @state[i] = :hit
      self
    end
    def set_miss
      raise "Bad input" if @state[i]
      @state[i] = :miss
      self
    end

    def set(info)
#pp 'info', info
      info.each_with_index do |s, i|
        case s
        when :miss
          set_miss(i)
        when :hit
          set_hit(i)
        when :exclude
          set_exclude
        else
          raise "unknow input"
        end
        self
      end
      self
    end
    def get_all
      @dl
    end
    def length
      @letters.length
    end
  end
end
__END__
      @letters.each do |l|
        x = Hash[ 0, :seen, 1, :seen, 2, :seen, 3, :seen, 4, :seen ]
        @dl.push Hash[ l: l, state: x ]
      end

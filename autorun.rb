#!/usr/bin/env ruby 
require 'wordle/autorun'
require 'getoptlong'

require 'pp'

require 'wordle/words'
require 'wordle/guess'
require 'sd'

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--reverse', '-r', GetoptLong::NO_ARGUMENT ],
  [ '--need',  GetoptLong::NO_ARGUMENT ],
  [ '--normalize', '-n', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--list', '-l', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--mf', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--debug', GetoptLong::REQUIRED_ARGUMENT ],
  [ '-d', GetoptLong::NO_ARGUMENT ],
  [ '-x', '--display', GetoptLong::REQUIRED_ARGUMENT],
  [ '--hard', '-R', GetoptLong::NO_ARGUMENT],
)

debug = 0
need = true
reverse = false
hard = false
normalize_list = Hash[
  'none', [ 1, 1, 1, 1, 1 ],
  'first', [ 4, 1, 1, 1, 1 ],
  'ld', [ 8, 4, 2, 1, 0 ],
  'wd', [ 13, 4, 8, 3, 1 ],
]
normalize = normalize_list['none']

mf = Hash[
  ('a'..'z').map { |s| [ s.to_sym, [2,1,1,1,1]] },
]

mf[:s][4] = Rational(1,4)

opts.each do |opt, arg|
  case opt
  when '--help'
    exit
  when '--debug'
  when '--list'
    case arg
    when 'normalize'
      puts normalize_list.keys.join ', '
    end
    exit
  when '--mf'
    arg.split(',').each do |k|
      if (d = k.match /([a-z])([0-4])=(.*)/)
puts "%s %s %s" % [ d[1], d[2], d[3] ]
	mf[d[1].to_sym][d[2].to_i] = d[3].to_r
      end
    end
  when '--normalize'
    t = normalize_list[arg] or raise "no normalize named '%s'" % [ arg ]
    normalize = t
  when '--need'
    need = false
  when '--reverse'
    reverse = true
  when '-x'
    pp mf
    exit
  when '-d'
    debug += 1
  when '-R', '--hard'
    hard = true
  end

end

#######################################################################################

words = Wordle::Words.new(reverse: reverse, normalize: normalize, mf: mf)

[  ].reverse.each do |w|
  words.words.unshift( words.hwords[w] )
end

list = Array.new

if ARGV.empty?
  list = words.raw_words
else
  list = ARGV
end

Autorun.run(list, words, need, debug, hard)


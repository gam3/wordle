require 'pp'
require 'set'

class Wordle

  class Guess
    EXCLUDE = :exclude
    MISS    = :miss
    HIT     = :hit

    attr_reader :count
    def initialize(words, need: false, debug: 0)
      @words = words
      @count = words.size
      @need = need
      @debug = debug
    end

    def hit
      @count == 1
    end

    def inspect
      { count: @count, match: @matches }
    end

    def words
      @words.words
    end

    def word(matches)
      xwords = words.lazy.select{ |d| exact_match(d, matches) }.first(900)

      grab = nil
      max = '' 
      a = 0
      b = 1
      from = ""
      if (5 - matches.match_count - matches.letter_count) <= 0
#         pp matches, matches.match_count, matches.letter_count
      else
        a = xwords.length / (5 - matches.match_count - matches.letter_count)
        b = 6 - matches.length
      end
      if a > b && @need  && matches.length < 5
        need = xwords.map do |w|
          w.letters.zip(matches.matched).keep_if { |v| v[1].nil? }.flatten.compact
        end.flatten
        n = need.compact.to_set
        matches.matched.compact.each do |a|
          n.delete(a)
        end
        if n.length > 1
          x = words.inject([0, nil]){ |acc, w| m = need_match(w, n.to_a, matches); m > acc[0] ? [m, w, acc[0], acc[1] ] : acc }
          if !x[1].nil?
            grab = x[1]
            from = "%d need" % [ x[0] ]
          end
        end
      end
      @count = 1000
      if grab
        word = grab
      elsif xwords.length == 1
        from = "exact"
        @count = 1
        word = xwords.first
      elsif xwords.length > 0
        from = "exact first"
        @count = xwords.length
        word = xwords.first
      else
        puts "count miss"
        pp self
        @count = 0
      end
      if (@debug > 0)
        puts "%10s %10s x%5d %s (%s %s %2s) %s %-5s %-d %2d %s" %
          [ word.to_s, from, xwords.length, max,
          matches.match_count, matches.letter_count, matches.exclude_list,
	  matches.matched.map{ |x| x or '-'}.join,
	  matches.need.sort.join,
          6 - matches.length,
          a,
	  word.value
          ]
      end
      return word
    end

    def update(input, word, matches)
puts "No don't do this"
      if input.length != 5
        raise "Bad input length"
      end
      match = Match.new(input, word)
      matches.add match

      self
    end

    def need_match(w, need, matches)
      count = 0
      seen = Set.new
      w.each_letter_with_index do |l,i|
        if need.include?(l) and !seen.include?(l) 
          count += 16
        end
	seen.add(l)
        case matches.match_posission(l,i)
        when :exclude
          count -= 1
        when :seen
          count -= 1
        when :hit
          count -= 4
        when :miss
          count -= 8
        when :unknown
        else
#         pp @matches.match_posission(l,i)
        end
      end
      return count;
    end

    def exact_match(w, matches)
      hit = 0
      miss = 0
      need = matches.needs

      w.each_letter_with_index do |l,i|
        need.remove?(l)
        state = matches.match_posission(l,i)
        case state
        when :exclude
          return false
        when :miss
          return false
        when :hit
          hit += 1
        when :seen
          miss += 1
        when :unknown
        else
        end
      end
      return hit == matches.match_count && need.empty?
    end
  end
end


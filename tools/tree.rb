#!usr/bin/evn ruby

require 'pp'
require 'set'

word_list =
begin
  file = File.open '../data/wordlist';
  file.map{ |x| x.chomp! }.to_set
end

root = ARGV.shift

def build(root, word_list,  n)
  letters = Hash.new { |h,k| h[k] = Set.new }

  word_list.each do |w|
    w.split('').uniq.each do |l|
      letters[l].add w
    end
  end

  ms = word_list.dup
  us = Set.new
  root.split('').uniq.each do |l|
    ms -= letters[l]
    us += letters[l]
  end
  puts "%d %s %s %s" % [ n, root, ms.size, us.size ]
  [ ms, us ]
end

sl = word_list.to_a.first(1000).to_set
roots = sl

(x, y) = build('fuzzy', sl, 0)
pp x.size, y.size
exit

__END__

roots.each do |root|
  pp root
  exit
end


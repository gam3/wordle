
require 'wordle/words'
require 'wordle/guess'
require 'sd'

class Autorun
  def self.run(list, words, need, debug, hard)
    sd = Sd.new
    fail = 0
    count = 0

    list.each do |w|
      h = Hash.new { |h,k| h[k] = Array.new(5, false) }
      w.split(//).each_with_index do |l, i|
        h[l][i] = true
      end

      guess = Wordle::Guess.new(words, need: need, hard: hard, debug: debug)

      while word = guess.word
        md = []
        count += 1
        word.letters.each_with_index do |l, i|
          if (h.include?(l))
            if h[l][i]
              md.push Wordle::Guess::HIT
            else
              md.push Wordle::Guess::MISS
            end
          else
            md.push Wordle::Guess::EXCLUDE
          end
        end
        puts "%s %s %s %s" % [ guess.index, word, md, w ] if debug > 1
	guess.update(md, word)
        if word.to_s == w
          ex = ''
          if guess.index > 6
            fail += 1;
            ex = 'FAIL'
          end
          sd.update( guess.count )
          puts "%s %s [ %5d %4.3f %4.3f ] %5d %s %9d (%s) %s" % [
	    w, guess.count, sd.n, sd.mean, sd.standard_deviation, count, fail, word.value,
	    word.values.map{ |v| "%4d" % v }.join(', '), ex ]
          break
        end
      end
    end
  end
end

__END__


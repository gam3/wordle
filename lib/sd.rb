
class Sd
  attr_reader :n
  def initialize(n: 0, mu: 0.0, sq: 0.0)
    @n = n
    @mu = mu
    @sq = sq
  end
  def update(x)
    @n += 1
    muNew = @mu + (x - @mu) / @n
    @sq += (x - @mu) * (x - muNew)
    @mu = muNew
    self
  end
  def delete(x)
    raise RuntimeError.new("Can't delete what isn't there!") if @n == 0
    if (@n == 1)
      @n = 0
      @mu = 0.0
      @sq = 0.0;
    else
      muOld = (@n * @mu - x) / (@n - 1)
      @sq -= (x - @mu) * (x - muOld)
      @mu = muOld;
      @n -= 1
    end
  end
  def mean()
    return @mu
  end
  def sample_variance 
    return @n > 1 ? @sq / (@n - 1) : 0.0
  end
  def standard_deviation()
    Math.sqrt(sample_variance())
  end
end

module Enumerable
  def sd
    sd = Sd.new()
    self.each do |x|
      sd.update(x)
    end
    return sd
  end
end

__END__
sd = 1.upto(10).sd
puts "x = %s %s " % [ sd.n, sd.standard_deviation ]

10.downto(0).each do |x|
  sd.delete(x)
  puts "x = %s %s " % [ sd.n, sd.standard_deviation ]
end


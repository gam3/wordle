#!/usr/bin/env ruby 
require 'wordle/autorun'
require 'getoptlong'

require 'pp'

require 'wordle/words'
require 'wordle/guess'
require 'wordle/matches'
require 'sd'

opts = GetoptLong.new(
  [ '--answer', '-a', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--reverse', '-r', GetoptLong::NO_ARGUMENT ],
  [ '--vowels', '-v', GetoptLong::NO_ARGUMENT ],
  [ '--need',  GetoptLong::NO_ARGUMENT ],
  [ '--normalize', '-n', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--list', '-l', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--language', '--lang', '-L', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--mf', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--debug', GetoptLong::REQUIRED_ARGUMENT ],
  [ '-d', GetoptLong::NO_ARGUMENT ],
  [ '--hard', '-H', GetoptLong::NO_ARGUMENT ],
  [ '--words', '-W', GetoptLong::REQUIRED_ARGUMENT ],
)

debug = 0
need = true
reverse = false
vowels = false
normalize_list = Hash[
  'none', [ 1, 1, 1, 1, 1 ],
  'first', [ 4, 1, 1, 1, 1 ],
  'ld', [ 8, 4, 2, 1, 0 ],
  'wd', [ 13, 4, 8, 3, 1 ],
]
normalize = normalize_list['none']

answer = nil

mf = Hash[
  ('a'..'z').map { |s| [ s.to_sym, [2,1,1,1,1]] },
]

#mf[:s][4] = Rational(1,4)
#mf[:s][0] = Rational(1,4)
#mf[:d][4] = Rational(1,8)

language = 'english'

used_words = Array.new

opts.each do |opt, arg|
  case opt
  when '--answer'
    answer = arg
    raise unless arg.length >= 5
    raise unless arg.length <= 10
    raise unless arg.match /[a-z.]/
  when '--help'
  when '--debug'
  when '--list'
    case arg
    when 'normalize'
      puts normalize_list.keys.join ', '
    end
    exit
  when '--language'
    language = arg
  when '--hard'
  when '--mf'
    arg.split(',').each do |k|
      if (d = k.match /([a-z])([0-4])=(.*)/)
puts "x %s %s %s" % [ d[1], d[2], d[3] ]
	mf[d[1].to_sym][d[2].to_i] = d[3].to_r
      end
    end
  when '--normalize'
    t = normalize_list[arg] or raise "no normalize named '%s'" % [ arg ]
    normalize = t
  when '--need'
    need = false
  when '--reverse'
    reverse = true
  when '--vowels'
    vowels = true
  when '-d'
    debug += 1
  when '--words'
    arg.split(',').each do |k|
      used_words.push Wordle::Word.new(k)
    end
  end
end

words = Wordle::Words.new(reverse: reverse, normalize: normalize, mf: mf, language: language, vowels: vowels)

######################################################################################################

guess = Wordle::Guess.new(words, need: need, debug: debug)
matches =  Wordle::Matches.new()

convert = {
    '+' =>  Wordle::Guess::MISS,
    'x' =>  Wordle::Guess::HIT,
    '-' =>  Wordle::Guess::EXCLUDE,
}

def help
  puts <<EOT
For exact match use the 'x' character.
For word  match use the '+' character.
For no match use the '-' or space character.
EOT
end

if used_words.size > 0
  matches.add_words(match_string: answer, words: used_words)
end

if (answer && !answer.match(/[a-z]{5}/))
  answer = nil
end

count = 0
while word = guess.word(matches)
  puts "Enter (%d): %s" % [ count + 1, word ]
  begin
    if answer
      response = word.compare(answer)
      puts response
    else
      print "Enter response: "
      response = gets.chomp
    end
    case response
    when 'xxxxx' 
      exit;
    when /^[-x+]{5}$/
      input = response.split(//).map { |c| convert[c]  }
      matches.add(input: input, word: word.chars)
      puts matches.to_s
    when 'help' 
      help
    when 'adump' 
      pp words.alfs
    when 'wdump' 
      pp words.words
    when 'dump' 
      pp matches
    else
      puts "Unknown input: '%s' try 'help'" % [ response ]
    end
  rescue StandardError => e
    puts e.inspect
    puts e.backtrace
    retry
  end
  count += 1
end

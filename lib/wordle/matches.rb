
require 'pp'
require 'set'

class Wordle
  class Alpha
    attr_reader :letter
    attr_reader :index
    attr_reader :state
    def initialize(l, i)
      @letter = l[1]
      @index = i
      @state = l[0]
    end
    
    def inspect
      "<Alpha l: %s i: %s s: %s >" % [ @letter, @index, @state ]
    end
  end

  class Match
    def initialize(a = [], b = [])
      hit = []
      miss = []
      a.each_with_index do |t, i|
        case t
        when :noop
          hit.push '.'
        when :hit
          hit.push b[i]
        when :miss
          hit.push '.'
          miss.push b[i]
        when :exclude
          hit.push '.'
        else
        end
      end
      @word = b.join('')
      @match_word = (hit + miss).join('');
    end

    def set_noop(a)
      @data[a.index][0] = :noop
    end
    def each
      @data.each_with_index do |a, i|
        yield Alpha.new(a, i)
      end
    end

    def convert(t)
      case t
      when :noop
        '.'
      when :hit
        'x'
      when :miss
        '+'
      when :exclude
        '-'
      end
    end

    def to_s
      return @match_word
    end
  end

  class Matches
    attr_reader :matched
    attr_reader :need

    def matched=(match)
      @matched = match.to_s.split(//)
      self
    end

    def new_letters(letters)
        count = 0
        letters.each do |l|
          count += 1 if @seen.include?(l)
        end
        return 5 - count;
    end

    class Need < Hash
      def initialize()
        replace(Hash.new { |h,k| h[k] = 0 })
      end

      def to_s
        ret = ''
        self.each do |k, v|
          ret += k * v
        end
        return ret
      end

      def empty?
        self.values.inject(0, :+) == 0
      end

      def add(c)
        self[c] += 1
      end

      def add?(c)
        ret = self[c]
        self[c] += 1
        return ret
      end

      def remove?(c)
        if (ret = self[c] > 0)
          self[c] -= 1
        end
        return ret
      end
    end

    def words
      @words
    end

    def dup
      @count = @count.dup
      @miss_count = @miss_count.dup
      @list = @list.dup
      @seen = @seen.dup
      @exclude = @exclude.dup
      @matched = @matched.dup
      @miss = @miss.dup
      @words = @words.dup
      self
    end

    def initialize(match_string: '.....', words: [])
      @count = Need.new { |h,k| h[k] = 0 }
      @miss_count = Hash.new { |h,k| h[k] = 0 }
      @list = Array.new
      @seen = Set.new
      @exclude = Set.new
      @need = Set.new
      @matched = Array.new(5, nil)
      @miss = Array.new(5) { Set.new }
      @words = []

      add_words(match_string: match_string, words: words)

    end

#    def pretty_print(pp)
#      pp.text to_s
#    end

    def exclude_list
      @exclude.length
    end

    def length
      @list.length
    end

    def match_count
      @matched.compact.length
    end

    def letter_count
      @need.length
    end

    def check(match)
      h = Set.new
      match.each do |a|
        if @matched[a.index] == a.letter and a.state == :hit
           h.add(a.letter)
        end
      end
      match.each do |a|
        if @matched[a.index] == a.letter
          raise "Bad state '%s' should be '%s'" % [ a.state.to_s, :hit.to_s ] unless a.state == :hit
        elsif @miss[a.index].include?(a.letter)
          if @count[a.letter]
            puts "XXX"
          else
            raise "Bad state '%s' should be '%s'" % [ a.state.to_s, :miss.to_s ] unless a.state == :miss
          end
        elsif @exclude.include?(a.letter)
          pp Hash[ m: match, a: a, s: self ]
          raise "Bad state '%s' should be '%s'" % [ a.state.to_s, :exclude.to_s ] unless a.state == :exclude
        end
      end
    end

    def fix!(match)
      match.each do |a|
        case a.state
        when :hit
          if @matched[a.index] == a.letter
            match.set_noop(a)
          end
        when :exclude
          if @exclude.include?(a.letter)
            match.set_noop(a)
          end
        when :miss
          if @miss[a.index].include?(a.letter)
            match.set_noop(a)
          end
        when :noop
          puts 'noop'
        end
        @seen.add a.letter
      end
      self
    end

    def undo
#      did = @list.pop
      puts "undo %s" % did
#      remove(did)
    end

    def remove(match)
      h = Hash.new { |h,k| h[k] = 0 }

      match.each do |a|
        next if a.state == :noop
        case a.state
        when :hit
          @matched[a.index] = nil
          h[a.letter] += 1
        when :exclude
          @exclude.delete(a.letter)
        when :miss
          @miss[a.index].delete(a.letter)
          h[a.letter] += 1
        end
      end

      h.each do |k, v|
        if @count[k] == v
          @count.delete(k)
        else
          puts "miss remove"
          pp match
        end
      end
      @seen = (@exclude.to_a + @count.keys).to_set
    end

    def add_word(match_string: '.....', word: nil)
      return nil unless word
      add_words(match_string: match_string, words: [ word ])
      self
    end

    def add_words(match_string: '.....', words: [])
      puts "add_words(%s, [ %s ]" % [ match_string, words.join(', ') ] if words.length > 0

      count = Need.new

      match_string.chars.each_with_index do |c, i|
        next unless /[a-z]/.match c
        raise "Bad input" if  @exclude.include?(c)
        if (i < 5)
          pp @matched if @matched[i] && @matched[i] != c
          raise "Bad input %s(%s) should be %s" % [ c, i, @matched[i] ] if @matched[i] && @matched[i] != c
          @matched[i] = c
        else
          @need.add c   
        end
        count.add(c)
        @seen.add(c)
      end

      words.each do |word|
        word.chars.each_with_index do |c, i|
          if count.include?(c)
            if @miss_count[c] + count[c] > 5
              pp self
              raise "Bad data: too many %s's" % [ c ]
            end
            unless @matched[i] == c;
              @miss_count[c] += 1 if !@miss[i].include?(c)
              @miss[i].add(c)
            end
          else
            @exclude.add(c)
          end
          @seen.add(c)
        end
        @words.push word
      end

      @miss_count.each do |k,v|
        if v == 4
          @miss.each_with_index do |l, i|
            unless l.include?(k)
               @matched[i] = k
            end
          end
        end
      end

      @count.merge!(count)
      self
    end

    def add(input: nil, word: nil)
      match = Match.new(input, word)
      add_words(match_string: match.to_s, words: [ word.join ])
      self
    end

    def needs()
      @count.dup()
    end

    def seen(l)
      @seen.include?(l)
    end

    def excluded(l)
      @exclude.include?(l)
    end

    def match_posission(l, i)
      ret = :unknown
      case
      when c = @matched[i]
        ret = :miss
        ret = :hit if c == l
      when excluded(l)
        ret = :exclude
      when @miss[i].include?(l)
        ret = :miss
      when seen(l) 
        ret = :seen
      else
      end
      return ret
    end

    def to_s
      count = @count.dup
      ret = '';
      @matched.each do |c|
        if c
          count[c] -= 1
          ret += c
        else
          ret += '.'
        end
      end
      count.each do |k,v|
        if v > 0
          ret += k * v
        end
      end
      ret
    end
  end

end


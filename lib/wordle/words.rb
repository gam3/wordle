require 'wordle/word'

class Wordle
  class Words
    attr_reader :alfs
    attr_reader :hwords
    def initialize(reverse: false, normalize: [ 1, 1, 1, 1, 1 ], mf: Hash.new, language: 'english', vowels: false)
      @hwords = Hash.new { |h,k| h[k] = Word.new(k) }
      @alfs = Hash.new { |h,k| h[k] = Array.new(5,0) }
      @normalize = normalize
      @reverse = reverse
      @vowels = vowels
      @mf = mf
      @word_list = Array.new

      File.open('data/wordlist.%s' % language).each_line do |line|
        word = line.chomp
        @word_list.push word
        raise if word.size != 5
      end

      @word_list.each do |w|
        w.chomp!
        w.split(//).each_with_index do |l, i|
	  @alfs[l][i] +=  @normalize[i]
        end
      end
      @word_list.each do |w|
        w.split(//).each_with_index do |l, i|
	  h = @mf.include?(l.to_sym) ? @mf[l.to_sym] : [1,1,1,1,1]
	  v = @alfs[l][i] * h[i]
          @hwords[w].add_value(v, i, l)
        end
      end

      if @reverse
	@ow = @hwords.values.sort_by { |v| v.value(true, @vowels) }
      else
	@ow = @hwords.values.sort_by { |v| 0 - v.value(false, @vowels).to_i }
      end
    end

    def size
      @ow.size
    end

    def words
      @ow
    end

    def raw_words
      @word_list
    end

  end
end

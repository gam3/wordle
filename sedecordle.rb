#!/usr/bin/env ruby 
require 'wordle/autorun'
require 'getoptlong'

require 'pp'

require 'wordle/words'
require 'wordle/guess'
require 'wordle/matches'
require 'sd'

opts = GetoptLong.new(
  [ '--answers', '-a', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--init', '-g', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--reverse', '-r', GetoptLong::NO_ARGUMENT ],
  [ '--vowels', '-v', GetoptLong::NO_ARGUMENT ],
  [ '--need',  GetoptLong::NO_ARGUMENT ],
  [ '--normalize', '-n', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--list', '-l', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--language', '--lang', '-L', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--mf', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--debug', GetoptLong::REQUIRED_ARGUMENT ],
  [ '-d', GetoptLong::NO_ARGUMENT ],
  [ '--size', '-s', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--words', '-W', GetoptLong::REQUIRED_ARGUMENT ],
)

debug = 0
need = true
reverse = false
vowels = false
normalize_list = Hash[
  'none', [ 1, 1, 1, 1, 1 ],
  'first', [ 4, 1, 1, 1, 1 ],
  'ld', [ 8, 4, 2, 1, 0 ],
  'wd', [ 13, 4, 8, 3, 1 ],
]
normalize = normalize_list['none']

answers = []
init = []

mf = Hash[
  ('a'..'z').map { |s| [ s.to_sym, [2,1,1,1,1]] },
]

mf[:s][4] = Rational(1,4)
language = 'english'
used_words = Array.new
size = 8

opts.each do |opt, arg|
  case opt
  when '--answers'
    answers = arg.split(/,/)
    answers.each do |a|
      raise 'Answer (%s) has bad size %s' % [ a, a.length ] unless a.length >= 5 and a.length <= 10
    end
  when '--init'
    init = arg.split(/,/)
    init.each do |a|
      raise 'Init (%s) has bad size %s' % [ a, a.length ] unless a.length >= 5 and a.length <= 10
    end
  when '--help'
  when '--debug'
  when '--list'
    case arg
    when 'normalize'
      puts normalize_list.keys.join ', '
    end
    exit
  when '--language'
    language = arg
  when '--size'
    size = arg.to_i
  when '--mf'
    arg.split(',').each do |k|
      if (d = k.match /([a-z])([0-4])=(.*)/)
puts "x %s %s %s" % [ d[1], d[2], d[3] ]
	mf[d[1].to_sym][d[2].to_i] = d[3].to_r
      end
    end
  when '--normalize'
    t = normalize_list[arg] or raise "no normalize named '%s'" % [ arg ]
    normalize = t
  when '--need'
    need = false
  when '--reverse'
    reverse = true
  when '--vowels'
    vowels = true
  when '-d'
    debug += 1
  when '--words'
    arg.split(',').each do |k|
      used_words.push k
    end
  end
end

raise "Not enough answers" unless answers.empty? || answers.length == size

words = Wordle::Words.new(reverse: reverse, normalize: normalize, mf: mf, language: language, vowels: vowels)

used_words = used_words.map { |w| words.hwords[w] }

######################################################################################################

convert = {
  '+' =>  Wordle::Guess::MISS,
  'x' =>  Wordle::Guess::HIT,
  '-' =>  Wordle::Guess::EXCLUDE,
}

def help
  puts <<EOT
For exact match use the 'x' character.
For word  match use the '+' character.
For no match use the '-' or space character.
EOT
end

class Input # < Wordle::Guess
    attr_writer :match
    def initialize(guess: nil, answer: nil, match: nil, i: 0)
       @guess = guess
       @answer = answer
       @exact = [ '.', '.', '.', '.', '.' ]
       @hit = [ ]
       @final = nil
       @match = match
       @i = i
    end

    def i
      @i
    end

    def response
      if @answer
        @answer
      else
        @match.to_s
      end
    end

    def match
      @match
    end

#    def done(word)
#      @final = word
#      @word = word.to_s
#    end

    def done?
      false
    end

    def check(input)
      ret = input;
      if (m = input.match /^(-|[.a-z]{5})([a-z]{0,5})$/)
        ret = true;
      elsif (m = input.match /^([x+-]{5})$/)
        ret = true;
        ret = 'bad';
      end
      return ret;
    end

    def word
      @guess.word(@match)
    end

    def guess
      @guess
    end
end

guesses = Array.new

1.upto(size).each do |i|
  match = nil
  if used_words.length > 0
    match = Wordle::Matches.new()
  else
    match = Wordle::Matches.new()
  end
  puts "init %s %s" % [ i, init[i-1] ] if init[i-1]
  x = Input.new(
    match: match,
    answer: init[i-1],
    guess: Wordle::Guess.new(words, need: need, debug: debug),
    i: i
  )
  guesses.push x
end

hits = Array.new
wordlist = Array.new

done = false

class Common < Hash
  attr_reader :word
  def initialize()
    replace(Hash.new { |h,k| h[k] = 0 })
    @max = 0;
  end

  def add(word)
    if self[word] == @max
      @max += 1
      @word = word
    end
    self[word] += 1
  end
end

while hits.size < size
  dolist = Array.new
  list = Array.new
  auto_answer = false
  dolist = used_words
  max = Common.new

  puts "Loop %s" % [ dolist.size ]
  guesses.each do |input|
    next if input.done?
    x = input.word

    next unless x
    puts "hits %s %s %s" % [ input.guess.count, x, input.match.new_letters(x.chars) ]
    if input.guess.hit
#      input.match.matched = input.word
      next if hits.include? x
      hits.push x
      if used_words.include?(x)
        puts "skipping %s" % x
      else
        puts "Hit: %s %s" % [ input.word, input.i ]
        dolist.push x if x
      end
    else
      if !used_words.include?(x)
        list.push x
        max.add(x)
      end
    end
  end

  if dolist.empty?
#    pp max
    puts "Guess: %s" % [ max.word ]
    dolist.push max.word if list.size > 0
  end
  used_words = Array.new

  dolist.each do |word|
    count = 0

    begin
      print "Word: %s " % [ word.to_s ]
      reply = gets

      case reply.chop
        when /^[a-z]{5}$/
          word = words.hwords[reply.chop]
          raise 'retry'
        when 'y', 'Y'
          puts "OK" 
        else
          raise "what"
      end
    rescue NameError => e
      puts e
      exit
    rescue  => e
      puts e.class
      retry
    end

    responses = Array.new
    updates = Array.new
    matches = Array.new(size)

    while (count < size)
      begin
        w = guesses[count]

        response = w.response
        auto = word.compare(response)

        print "Enter response (%d) (%s) [%s]: " % [ count + 1, response, auto ]
        if w.done?
          count += 1
          puts
          next
        end
        begin

          if response[0..4].gsub(/\./, '').chars.length == 5
            answer = 'y'
          else
            answer = gets.chomp
          end
        rescue => e
          puts "RESCUE " + e.to_s
          exit;
        end

        if (answer == 'y')
          answer = auto
        end

        case answer
        when /[-+x]{5}/

          md = answer.split(//).map { |c| convert[c]  }
          wd = word.chars

          match = w.match.dup
          response = match.to_s
          responses.push response

          matches[count] = match
#pp matches

          w.match.add(input: md, word: word.chars)

          puts "Answer: %s Response: %s - %s" % [ answer, w.match.to_s, w.response ]

          count += 1
        when 'list'
          wordlist.each do |w|
            puts w
          end
        when 'show'
          pp w
        when 'print'
          puts "./sedecordle --answers=%s --words=%s" % [ guesses.map { |g| g.match.to_s }.join(','), wordlist.join(',') ]
        when 'quit'
          exit
        when 'back', 'undo'
          ncount = (count - 1) % size
          if !matches[ncount]
             puts "nothing to undo"
          else
#pp matches
             puts "shit to undo"
             w = guesses[ncount]
             pp w.match,  matches[ncount]
             w.match = matches[ncount]
             pp w.match
             count = ncount
          end
          redo
        else
          puts "Unknown input: '%s' try 'help'" % [ answer ]
          redo
        end
      rescue NoMethodError => e
        raise e
      rescue StandardError => e
        puts e.inspect
        puts e.backtrace
        retry
      end
    end
    wordlist.push word.to_s

    puts "./sedecordle --answers=%s --words=%s" % [ guesses.map { |g| g.match.to_s }.join(','), wordlist.join(',') ]

  end
end

__END__


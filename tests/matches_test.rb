
require 'minitest/autorun'
require 'wordle/matches'

class MatchesTest < Minitest::Test

  def test_initialize
    item = Wordle::Matches.new();
    assert_instance_of(Wordle::Matches, item)
  end

  def test_initialize_args
    item1 = Wordle::Matches.new(match_string: '.....ab', words: ['zaaaa', 'zbbbb', 'zebra'])
    item2 = Wordle::Matches.new()
    assert_instance_of(Wordle::Matches, item1)
    assert_instance_of(Wordle::Matches, item2)
  end

  def test_initialize_args_fail
    e = assert_raises(ArgumentError) {
      Wordle::Matches.new(gues: 'bozos')
    }
    assert_equal('unknown keyword: :gues', e.message)
  end

  def test_force_on_misses
    item = Wordle::Matches.new(match_string: '.....ab', words: ['aaaza', 'zbbbb'])
    assert_equal('b..a.', item.to_s, 'match string')
    item1 = Wordle::Matches.new();
    item1.add(input: [:miss, :exclude, :exclude, :exclude, :exclude], word: 'aaaza'.chars)
    item1.add(input: [:exclude, :miss, :exclude, :exclude, :exclude], word: 'zbbbb'.chars)
#    pp item1, item
#    assert_equal(item1, item)
    e1 = assert_raises(RuntimeError) {
      item.add(input: [:hit, :miss, :miss, :miss, :miss], word: 'about'.chars)
    }
    e2 = assert_raises(RuntimeError) {
      item1.add(input: [:hit, :miss, :miss, :miss, :miss], word: 'about'.chars)
    }
    assert_equal(e1.to_s, e2.to_s)
  end

  def test_tsuba
    item = Wordle::Matches.new(match_string: '.....', words: [])
    puts item.to_s
    item.add_word(match_string: '.....s', word: 'ijsco')
    puts item.to_s
    item.add_word(match_string: '.....tsa', word: 'tsuba')
    puts item.to_s
    item.add_word(match_string: '.....tsa', word: 'stras')
    puts item.to_s
    
  end

end


